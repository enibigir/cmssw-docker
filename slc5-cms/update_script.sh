yum -y install make
yum -y groupinstall 'Development Tools'

export INSTALL_DIR=${UPDATE_PATH}
export GETTEXT_VERSION=gettext-0.20.1
export CURL_VERSION=curl-7.65.0
export OPENSSL_VERSION=openssl-1.1.1g
export M4_VERSION=m4-1.4.18
export AUTOCONF_VERSION=autoconf-2.69
export GIT_VERSION=git-2.21.0
export PERL_VERSION=perl-5.30.2
export LD_LIBRARY_PATH=${INSTALL_DIR}/lib:${LD_LIBRARY_PATH}
export PATH=${INSTALL_DIR}/bin:${PATH}
# mkdir -p ${INSTALL_DIR}
cd ${INSTALL_DIR}

wget http://ftp.gnu.org/pub/gnu/gettext/${GETTEXT_VERSION}.tar.gz
wget ftp://mirrors.gethosted.online/curl-haxx/${CURL_VERSION}.tar.gz
wget http://www.openssl.org/source/${OPENSSL_VERSION}.tar.gz
wget http://mirrors.edge.kernel.org/pub/software/scm/git/${GIT_VERSION}.tar.gz
wget http://ftp.gnu.org/gnu/m4/${M4_VERSION}.tar.gz
wget http://ftp.gnu.org/gnu/autoconf/${AUTOCONF_VERSION}.tar.gz
wget http://www.cpan.org/src/5.0/${PERL_VERSION}.tar.gz

tar xzf ${GETTEXT_VERSION}.tar.gz
cd ${GETTEXT_VERSION}
./configure --prefix=${INSTALL_DIR}
make
make install
cd ..
rm ${GETTEXT_VERSION}.tar.gz
rm -rf ${GETTEXT_VERSION}

tar -xzf ${PERL_VERSION}.tar.gz
cd ${PERL_VERSION}
./Configure -des
make
make install
cd ..
rm ${PERL_VERSION}.tar.gz
rm -rf ${PERL_VERSION}

tar xzf ${OPENSSL_VERSION}.tar.gz
cd ${OPENSSL_VERSION}
CFLAGS=-fPIC ./config --prefix=${INSTALL_DIR} shared zlib
make
make install
cd ..
rm ${OPENSSL_VERSION}.tar.gz
rm -rf ${OPENSSL_VERSION}

tar xzf ${CURL_VERSION}.tar.gz
cd ${CURL_VERSION}
./configure --with-ssl=${INSTALL_DIR} --prefix=${INSTALL_DIR}
make
make install
cd ..
rm ${CURL_VERSION}.tar.gz
rm -rf ${CURL_VERSION}

tar xzf ${M4_VERSION}.tar.gz
cd ${M4_VERSION}
./configure --prefix=${INSTALL_DIR}
make -j16
make install
cd ..
rm ${M4_VERSION}.tar.gz
rm -rf ${M4_VERSION}

tar xzf ${AUTOCONF_VERSION}.tar.gz
cd ${AUTOCONF_VERSION}
./configure --prefix=${INSTALL_DIR}
make -j16
make install
cd ..
rm ${AUTOCONF_VERSION}.tar.gz
rm -rf ${AUTOCONF_VERSION}

tar xzf ${GIT_VERSION}.tar.gz 
cd ${GIT_VERSION}
make configure
./configure --prefix=${INSTALL_DIR} --with-openssl=${INSTALL_DIR} --with-curl=${INSTALL_DIR}
make -j16
make install -i
cd ..
rm ${GIT_VERSION}.tar.gz
rm -rf ${GIT_VERSION}
